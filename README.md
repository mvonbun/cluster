# Cluster Simulation Scripts
Use these scripts to run clustered simulations.

## Installation
Run `install.sh` and follow the instructions to install the cluster
scripts on your system.

## Usage
Assume you have a file `parameters.txt` that is accessible by all your
simulation hosts.  This file should have **one set of parameters per line**.

Also, the main scripts `get_parameter` and `cluster_fifo` must be
accessible by all your simulation hosts.  Then, use `get_parameter` to
get a new set of parameters and pass this to your simulation routine.

Use the provided template `simulation_template.sh` to get a skeleton
using a while loop that constantly requests a new parameter set until an
empty set is returned.  The template does also provide pushing failed
runs back to the stack.  Note, however, that all runs should be able to
succeed, but not on all simulation hosts, or all your clients will exit
the cluster.

For MATLAB `getParameters.m` provides a function wrapper to
`get_parameter` and `pushParameters.m` provides a function wrapper to
push values back to the stack.

## Files
- `install.sh` Installation script.

### Core Functions
- `cluster_fifo.sh` Main locking script to push and pop.
- `get_parameter.sh` Interface that waits upon lock-release to get a new
  parameter.
- `lock_and_pop_line_bottom.sh` Old main locking parameter pop script.

### Auxiliary Scripts
- `pop_enable.sh` Script to reenable stopped parameter popping.
- `pop_disable.sh` Script to disable parameter popping.

### Interface / Template Functions
- `getParameters.m` MATLAB wrapper function (place in MATLAB search path
  using `addpath(/path/to/getParameters.m)`).
- `pushParameters.m` MATLAB wrapper function (place in MATLAB search path
  using `addpath(/path/to/getParameters.m)`).
- `simulation_template.sh` Template for clustered simulation (e.g. for
  using Docker).

