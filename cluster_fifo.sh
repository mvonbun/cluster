#!/bin/bash

# SOURCES:
# https://linuxaria.com/howto/linux-shell-introduction-to-flock

set -e

SCRIPT_NAME="$(basename "$0")"

# if using symlinks, this is the symlink file
# SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd -P)"
# resolve simlinks
SCRIPT_SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SCRIPT_SOURCE" ]; do # resolve $SCRIPT_SOURCE until the file is no longer a symlink
  SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"
  SCRIPT_SOURCE="$(readlink "$SCRIPT_SOURCE")"
  [[ $SCRIPT_SOURCE != /* ]] && SCRIPT_SOURCE="$SCRIPT_DIR/$SCRIPT_SOURCE" # if $SCRIPT_SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"

FILE_LOCKED_ID=64

function usage {
    echo "${SCRIPT_NAME} [--help] [--flock] [--record, --pop[=NUM], --push=WHAT] file"
    echo "Locking access to file."
    echo "Exits with state ${FILE_LOCKED_ID} if file is locked."
}


function help {
    usage
    echo ""
    echo "If the provided file is not locked, applies a lock to the file before" \
	 "popping the last NUM lines or pushing WHAT to the end of the file."
    echo ""
    echo "--pop[=NUM]      Pop the last NUM lines from the file. Defaults to 1."
    echo "--push=WHAT      Push WHAT to the end of the file."
    echo "-r / --record    Record who (user, host, time) used the fifo."
    echo ""
    echo "-h / --help      Show this help text and exit."
    echo ""
    echo "--flock          Use flock for locking (might not work on NFS)" \
	 "instead of the default which uses mv."
    echo ""
    echo "If both push and pop is provided, pop goes before push."
    echo ""
    echo "Exit codes used:"
    echo "   1    all exceptions."
    echo "   ${FILE_LOCKED_ID}   active lock"
}


if [ "$#" -lt 1 ]; then
    usage
    exit 1
fi


# ui
HELP=false
POPDEFAULT=true
POP=false
POPNUM=1
PUSH=false
PUSHWHAT=""
RECORD=false
USEFLOCK=false
## parser loop
while [[ "$1" == -* ]]; do
    case "$1" in
        -h | --help)
	    HELP=true;;
	-r | --record)
	    RECORD=true;;
	--pop)
	    POP=true;;
	--pop=*)
	    POP=true;
	    POPNUM=${1##*=};;
	--push=*)
	    PUSH=true;
	    POPDEFAULT=false;
	    PUSHWHAT=${1##*=};;
	--flock)
	    USEFLOCK=true;;
	--*)
	    echo "Unknown option: $1"; exit;;
        --)
	    shift; break;; # end of options
    esac
    shift
done


if [ "$HELP" == true ]; then
    help
    exit
fi

if [ "$POP" == false ]; then
    POP=$POPDEFAULT
fi


FILE_DIR="$(cd "$(dirname "$1")" && pwd )"
FILE_NAME="$(basename "$1")"
FILE="${FILE_DIR}/${FILE_NAME}"

RECORDFILE="${SCRIPT_DIR}/${FILE_NAME%%.*}_pop.log"
# LOCK="${SCRIPT_REALDIR}/${FILE_NAME%%.*}.lock"
# RECORDFILE="${SCRIPT_REALDIR}/${FILE_NAME%%.*}.lo


# try to lock; return FILE_LOCKED_ID otherwise
if [ "$USEFLOCK" == true ]; then
    LOCK="${SCRIPT_DIR}/${FILE_NAME%%.*}.lock"
    exec 200>"$LOCK"
    flock -n 200 || exit $FILE_LOCKED_ID
else
    mv "${FILE}" "${FILE}~" 2>/dev/null || exit $FILE_LOCKED_ID
    RESTORE="${FILE}"
    FILE="${FILE}~"
fi


# pop
if [ "$POP" == true ]; then
    if [ "$POPNUM" -gt 0 ]; then
	# get last NUM lines
	POPLINES=$(tail -"$POPNUM" "${FILE}")

	if [ "$POPLINES" != "" ]; then
	    # delete last POPNUM lines
	    if [ "$POPNUM" -eq 1 ]; then
		sed -i '$d' "${FILE}"
	    else
		sed -i -e :a -e "\$d;N;2,${POPNUM}ba" -e 'P;D' "${FILE}"
	    fi
	fi

	# record
	if [ "$RECORD" == true ]; then
	    TMP=($POPLINES)
	    echo "POP ${TMP[*]} ${USER}@${HOSTNAME} $(date +"%Y-%m-%d %H:%M:%S")" >> "$RECORDFILE"
	fi
	
	# return lines
	echo "$POPLINES"
    else
	echo "Invalid input. Number of parameters requested is $POPNUM."
	exit 1
    fi
fi


# push
if [ "$PUSH" == true ]; then
    if [ "$PUSHWHAT" != "" ]; then
	echo "${PUSHWHAT}" >> "$FILE"

	# record
	if [ "$RECORD" == true ]; then
	    TMP=($PUSHWHAT)
	    echo "PUSH ${TMP[*]} ${USER}@${HOSTNAME} $(date +"%Y-%m-%d %H:%M:%S")" >> "$RECORDFILE"
	fi

    else
	echo "Invalid input. Push empty line."
	exit 1
    fi
fi


# release lock
if [ "$USEFLOCK" == true ]; then
    # flock releases when process/script ends
    exit 0
else
    mv "${FILE}" "${RESTORE}" || exit 0
fi


