#!/bin/bash

# For use from local machine.
# fetch_data.sh subfolder

LOCALUSER="mivo"
LOCALDIR="/home/mivo/dev/noc/omnet/bufferless/fountain_noc/simulations/results/UniformFlag/${1}"

REMOTEUSER="ne45paz"
REMOTEDIR="/scratch/ne45paz/fountain_noc/simulations/results/UniformFlag/current/"

OPTIONS="-aunvP"

# get simulation hosts from log file
# for k in $(seq -f "%02g" 29 53); do echo -n "stu$k: "; grep -c "stu$k" runs.log; done
for worker in $(seq -f "%02g" 29 53); do
    cluster="stu"
    if [ "$worker" -ne 32 ] \
	   && [ "$worker" -ne 33 ] \
	   && [ "$worker" -ne 34 ] \
	   && [ "$worker" -ne 36 ] \
	   && [ "$worker" -ne 38 ]; then
	echo -n "${cluster}${worker}:"
	num_files=$(ssh -n "${cluster}${worker}" "ls -l ${REMOTEDIR} | wc -l")
	if [ "$?" -eq 0 ] && [ "$num_files" -gt 0 ]; then
	    ssh -n "${cluster}${worker}"\
		"echo ${OPTIONS} ${REMOTEDIR}/ ${LOCALUSER}@\${SSH_CONNECTION%% *}:${LOCALDIR}/"
	    ssh -n "${cluster}${worker}"\
		"rsync ${OPTIONS} ${REMOTEDIR}/*.sca ${LOCALUSER}@\${SSH_CONNECTION%% *}:${LOCALDIR}/"
	fi
    # else
	# echo "Skipping ${cluster}${worker}"
    fi
done
echo ""


# get simulation hosts from log file
# for k in $(seq -f "%02g" 1 18); do echo -n "prakt$k: "; grep -c "prakt$k" runs.log; done
for worker in $(seq -f "%02g" 1 18); do
    cluster="prakt"
    if [ "$worker" -ne 07 ]; then
	echo -n "${cluster}${worker}:"
	num_files=$(ssh -n "${cluster}${worker}" "ls -l ${REMOTEDIR} | wc -l")
	if [ "$?" -eq 0 ] && [ "$num_files" -gt 0 ]; then
	    ssh -n "${cluster}${worker}" \
		"rsync ${OPTIONS} ${REMOTEDIR}/*.sca ${LOCALUSER}@\${SSH_CONNECTION%% *}:${LOCALDIR}/"
	fi
    # else
	# echo "Skipping ${cluster}${worker}"
    fi
done
echo ""
