function [ parameter, varargout ] = getParameters( parameter_file, varargin )
%GETPARAMETERS Pop parameters from the stack using the system cluster
%scripts.
%
%   [ parameter, [hash] ] = GETPARAMETERS( parameter_file ) returns the
%   parameters of the bottom line. Additionally, returns a scalar hash in
%   the range [0, 2^32-1] as well.
%
%   [ parameter, [hash] ] = GETPARAMETERS( ..., 'record', [true/false] )
%   control if parameter popping should be logged or not. Defaults to true.
%
%   [ parameter, [hash] ] = GETPARAMETERS( ..., 'hash', ['sum', 'prod', function_handle] )
%   controls algorithm for the hash. Either uses the presets sum or prod of
%   a function handle (must accept vector inputs and provide a scalar
%   output).
%
%   [ parameter, [hash] ] = GETPARAMETERS( ..., 'hash_on_char', [true/false] )
%   controls if the hash algorithm works on the code value of a char (i.e.
%   '1' is interpreted as 49) or its double-meaning (i.e. '1' is 1).
%
%   Get the cluster scripts from
%   bitbucket.org:mvonbun/cluster.git
%
%   See also: pushParameters

% return state
PARAMETER_VALID = 0;
POP_DISABLED = 65;
FILE_NOT_FOUND = 66;
SYSTEM_ERROR = 127;

% for testing
% parameter_file = '$HOME/scripts/cluster/matlab.txt';
input_parser = inputParser;
addParameter(input_parser, 'record', false, ...
    @(x) validateattributes(x, {'numeric', 'logical'}, {'scalar'}));
addParameter(input_parser, 'hash', [], ...
    @(x) validateattributes(x, {'char', 'function_handle'}, {'vector'}));
addParameter(input_parser, 'hash_on_char', false, ...
    @(x) validateattributes(x, {'logical'}, {'scalar'}));
parse(input_parser, varargin{:});
record = logical(input_parser.Results.record);
hash_function = input_parser.Results.hash;
hash_on_char = input_parser.Results.hash_on_char;

% get line from file
if record
    [stat, para] = system(sprintf('get_parameter --record %s', ...
        parameter_file));
else
    [stat, para] = system(sprintf('get_parameter %s', ...
        parameter_file));
end

% check return status
switch stat
    case PARAMETER_VALID
        
    case POP_DISABLED
        warning('getParameters:Popping from %s is disabled.', parameter_file);
    case FILE_NOT_FOUND
        error('getParameters:input', ...
            'Parameter file %s not found.', parameter_file);
    case SYSTEM_ERROR
        [found, ~] = system('which get_parameter');
        if found == 1
            error('getParameters:function', ...
                'Function get_parameter not found.');
        elseif found == 0
            error('getParameters:function', ...
                'Function get_parameter but returned error %d.', stat);
        else
            error('getParameters:function', ...
                'which get_parameter returned %d.', found);
        end
    otherwise
        error('getParameters:unknown', ...
            'System call to get_parameter returned %d.', stat);
end

% remove last cntrl char (newline)
if isstrprop(para(end), 'cntrl')
    para(end) = '';
end

% isstrprop(para, 'digit')
% isstrprop(para, 'cntrl')

% return parameters and simple hash
if ~isempty(para) && (stat == 0)
    p = strsplit(para);
    parameter = cellfun(@(x) toarray(x), p);
    
    if nargout == 2
        % calculate the base of the hash
        if hash_on_char
            hash_base = double(para);
        else
            hash_base = parameter;
        end
        
        % execute hash computation
        if ischar(hash_function)
            switch hash_function
                case 'sum'
                    hash = sum(hash_base(:));
                case 'prod'
                    hash = prod(hash_base(:));
                otherwise
                    warning('Unknown hash function preset %s.', hash_function);
                    hash = 0;
            end
        else
            hash = hash_function(hash_base);
        end
        
        % ensure hash \in [0, 2^32-1]
        varargout{1} = mod(hash, 2^32-1);
    end
else
    parameter = [];
    if nargout == 2
        varargout{1} = [];
    end
end


    function x = toarray(x)
        if x == '-'
            x = -1;
        elseif ~isempty(x)
            x = str2double(x);
        end
    end
end

