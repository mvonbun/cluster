#!/bin/bash

SCRIPT_NAME="$(basename "$0")"

# if using symlinks, this is the symlink file
# SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd -P)"
# resolve simlinks
SCRIPT_SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SCRIPT_SOURCE" ]; do # resolve $SCRIPT_SOURCE until the file is no longer a symlink
  SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"
  SCRIPT_SOURCE="$(readlink "$SCRIPT_SOURCE")"
  [[ $SCRIPT_SOURCE != /* ]] && SCRIPT_SOURCE="$SCRIPT_DIR/$SCRIPT_SOURCE" # if $SCRIPT_SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"


function usage {
    echo "${SCRIPT_NAME} [--help] [--user USERNAME]  [--name NAME n1 [n2 ... nN]] [--test TEST-CMD] [--file FILENAME]"
    echo "Tests ssh connection to all clients NAMEn1, ..., NAMEnN by executing TEST-CMD on the remote client."
}


function help {
    usage
    echo ""
    echo "-h / --help                              Show this help text."
    echo "-u / --user USERNAME                     Login name."
    echo "-n / --name BASENAME POST_1 ... POST_N   Client base name and list of postfixes."
    echo "-t / --test TESTCMD                      Command to execute on the remote host to test simulation readiness."
    echo "-f / --file FILENAME                     Output filename."
    echo ""
    echo "Example:"
    echo "${SCRIPT_NAME} --name stu \$(seq 28 52) --name prakt \$(seq -f \"%02g\" 1 18) --test \"cd /scratch/ne45paz/fountain_noc\""
    echo "   tests if cding into folder /scratch/ne45paz/fountain_noc works on hosts stu28 - stu52 and prakt01 - prakt18."
}


### simple ui (no POSIX support)
## default values
HELP=false
TESTCMD="ls"
declare -a CLIENTS
CLIENTNAME=""
FILENAME=""
USER=""
## parser loop
while [[ "$1" == -* ]]; do
    case "$1" in
        -h | --help)
	    HELP=true;;
	-t | --test)
	    TESTCMD="$2"; shift;;
	-n | --name)
	    CLIENTNAME="$2"; shift;
	    while [[ "$2" != "" ]]; do
		if [[ "$2" == -* ]]; then
		    break
		fi
		CLIENTS[${#CLIENTS[*]}]="${CLIENTNAME}${2}"; shift;
	    done
	    ;;
	-f | --file)
	    FILENAME="$2"; shift;;
	-u | --user)
	    USER="$2"; shift;;
	-*)
	    echo "Unknown option $1"; exit 1;;
        --)
	    shift; break;; # end of options
    esac
    shift
done

# user input processing
if [ "$HELP" == true ]; then
    help
    exit
fi

if [ "${#CLIENTS[@]}" -eq 0 ]; then
    echo "Invalid input. No clients provided."
    echo ""
    usage
    exit 1
fi


# main
declare -a VALIDCLIENTS

for client in ${CLIENTS[@]}; do
    if [ "$USER" == "" ]; then
	ssh "${client}" "${TESTCMD}" &> /dev/null
    else
	ssh "${USER}@${client}" "${TESTCMD}" &> /dev/null
    fi

    if [ "$?" -eq 0 ]; then
	VALIDCLIENTS[${#VALIDCLIENTS[*]}]="${client}"
    fi
done


# output
if [ "${FILENAME}" == "" ] || [ -f "${FILENAME}" ]; then
    echo "${VALIDCLIENTS[@]}"
else
    echo "${VALIDCLIENTS[@]}" >> "${FILENAME}"
fi


# echo "${CLIENTNAME} :: ${CLIENTS[@]}"
