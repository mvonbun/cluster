#!/bin/bash

SCRIPT_NAME="$(basename "$0")"
# SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"

# script constants
FILE_LOCKED_ID=64
POP_DISABLE_ID=65
FILE_NOT_FOUND_ID=66
DISABLE_FILE_EXT="stop"

function usage {
    echo "${SCRIPT_NAME} [--help, --number, --record] parameter_file"
    echo "returns a (locking) new parameter (line) from the file provided."
    echo ""
    echo "To stop / disable the parameter popping for parameter_file,"
    echo "place aparameter_file_base.stop in the same location"
    echo "as the parameter file provided."
}

function help {
    usage
    echo ""
    echo "-h / --help         Show this help text."
    echo "-n / --number NUM   Return NUM parameters (default=1)."
    echo "-r / --record       Record the sequence of parameter popping by writing \"\$USER@\$HOSTNAME DATE TIME PARAMETER\" to a log file."
    echo ""
    echo "Exit codes are:"
    echo "    0: valid parameter set retrieved"
    echo "   ${POP_DISABLE_ID}: popping is disabled (parameter_file_base.stop file found)"
    echo "   ${FILE_NOT_FOUND_ID}: parameter_file not found"
}

if [ "$#" -lt 1 ]; then
    usage
    exit
fi

# ui
HELP=false
RECORD=false
NUM=1
## parser loop
while [[ "$1" == -* ]]; do
    case "$1" in
        -h | --help)
	    HELP=true;;
	-n | --num*)
	    NUM="$2"; shift;;
        -r | --record)
	    RECORD=true;;
	--*)
	    echo "Unknown option: $1"; exit;;
        --)
	    shift; break;; # end of options
    esac
    shift
done


if [ "$HELP" == true ]; then
    help
    exit
fi


PARAMETERFILE_DIR="$(cd "$(dirname "$1")" && pwd)"
PARAMETERFILE_NAME="$(basename "$1")"
PARAMETERFILE_BASE=${PARAMETERFILE_NAME%%.*}

# check for parameter_file
if [ -f "${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME}" ]; then
    # try to start popping only if there is not a *.stop file
    if [ -f "${PARAMETERFILE_DIR}/${PARAMETERFILE_BASE}.${DISABLE_FILE_EXT}" ]; then
	TRY_EN=$POP_DISABLE_ID
    else
	TRY_EN=$FILE_LOCKED_ID
    fi
else
    TRY_EN=$FILE_NOT_FOUND_ID
fi

# try to pop parameter and retry after [0.1,0.9] seconds if file is locked
while [ "$TRY_EN" -eq "$FILE_LOCKED_ID" ]; do
    if [ "$RECORD" == true ]; then
	PARAMETER=$(cluster_fifo --pop="$NUM" --record "${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME}")
    else
	PARAMETER=$(cluster_fifo --pop="$NUM" "${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME}")
    fi
    TRY_EN=$?
    if [ "$TRY_EN" -eq "$FILE_LOCKED_ID" ]; then
	sleep "0.$(((RANDOM % 9)+1))"
    fi
    if [ -f "${PARAMETERFILE_DIR}/${PARAMETERFILE_BASE}.${DISABLE_FILE_EXT}" ]; then
	TRY_EN=$POP_DISABLE_ID
    fi
done

# return popped parameter set if cluster_fifo return value is 0
if [ "$TRY_EN" -eq 0 ]; then
    echo "$PARAMETER"
else
    exit $TRY_EN
fi
