#!/bin/bash

# install.sh
# install.sh path/of/installation

script_name="$(basename "$0")"
script_dir="$( cd "$( dirname "$0" )" && pwd )"

function help {
    echo "${script_name} [path/of/installation]"
    echo "Install cluster scripts."
}

if [ "$1" == "--help" ]; then
    help
    exit
fi

if [ "$#" -eq 0 ]; then
    install_dir="$script_dir"
else
    if [ "$#" -eq 1 ]; then
	install_dir="$( cd "$1" && pwd )"
    else
	echo "Invalid input. Too many parameters."
	echo ""
	help
	exit 1
    fi
fi

mkdir --parents "${install_dir}"
ln -s "${script_dir}/get_parameter.sh" "${install_dir}/get_parameter"
ln -s "${script_dir}/lock_and_pop_line_bottom.sh" "${install_dir}/lock_and_pop_line_bottom"
ln -s "${script_dir}/cluster_fifo.sh" "${install_dir}/cluster_fifo"
ln -s "${script_dir}/pop_enable.sh" "${install_dir}/pop_enable"
ln -s "${script_dir}/pop_disable.sh" "${install_dir}/pop_disable"

echo "To complete installation, make sure that path"
echo "   ${install_dir}"
echo "is in your PATH variable. If not, add"
echo "   export PATH=\$PATH:${install_dir}"
echo "to your .bashrc."

