#!/bin/bash

# scriptname file

# https://linuxaria.com/howto/linux-shell-introduction-to-flock

set -e


SCRIPT_NAME="$(basename "$0")"

# if using symlinks, this is the symlink file
# SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd -P)"
# resolve simlinks
SCRIPT_SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SCRIPT_SOURCE" ]; do # resolve $SCRIPT_SOURCE until the file is no longer a symlink
  SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"
  SCRIPT_SOURCE="$(readlink "$SCRIPT_SOURCE")"
  [[ $SCRIPT_SOURCE != /* ]] && SCRIPT_SOURCE="$SCRIPT_DIR/$SCRIPT_SOURCE" # if $SCRIPT_SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"


function usage {
    echo "${SCRIPT_NAME} [--help, --number, --record] parameter_file"
    echo "if the provided file is not locked, locks the file and returns a new parameter (line) from the file."
    echo "Exits with state 64 if file is locked already."
    echo "If the file is empty, returns an empty string."
}


function help {
    usage
    echo ""
    echo "-h / --help         Show this help text."
    echo "-n / --number NUM   Return NUM parameters (default=1)."
    echo "-r / --record       Record who (user, host, time) got the parameter."
}


if [ "$#" -lt 1 ]; then
    usage
    exit
fi


# ui
HELP=false
RECORD=false
NUM=1
## parser loop
while [[ "$1" == -* ]]; do
    case "$1" in
        -h | --help)
	    HELP=true;;
	-n | --num*)
	    NUM="$2"; shift;;
        -r | --record)
	    RECORD=true;;
	--*)
	    echo "Unknown option: $1"; exit;;
        --)
	    shift; break;; # end of options
    esac
    shift
done

if [ "$HELP" == true ]; then
    help
    exit
fi


# pop
if [ "$RECORD" == true ]; then
    cluster_fifo --pop="$NUM" "$1"
else
    cluster_fifo --pop="$NUM" --record "$1"
fi

# return with pop exit state
exit $?

