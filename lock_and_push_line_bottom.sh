#!/bin/bash

# scriptname file

# https://linuxaria.com/howto/linux-shell-introduction-to-flock

set -e


SCRIPT_NAME="$(basename "$0")"

# if using symlinks, this is the symlink file
# SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd -P)"
# resolve simlinks
SCRIPT_SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SCRIPT_SOURCE" ]; do # resolve $SCRIPT_SOURCE until the file is no longer a symlink
  SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"
  SCRIPT_SOURCE="$(readlink "$SCRIPT_SOURCE")"
  [[ $SCRIPT_SOURCE != /* ]] && SCRIPT_SOURCE="$SCRIPT_DIR/$SCRIPT_SOURCE" # if $SCRIPT_SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"


function usage {
    echo "${SCRIPT_NAME} [--help, --record] file line-to-append"
    echo "if the provided file is not locked, locks the file and appends the line to the file."
    echo "Exits with state 64 if file is locked already."
}


function help {
    usage
    echo ""
    echo "-h / --help         Show this help text."
}


if [ "$#" -lt 1 ]; then
    usage
    exit
fi


# ui
HELP=false
RECORD=false
## parser loop
while [[ "$1" == -* ]]; do
    case "$1" in
        -h | --help)
	    HELP=true;;
	-r | --record)
	    RECORD=true;;
	--*)
	    echo "Unknown option: $1"; exit;;
        --)
	    shift; break;; # end of options
    esac
    shift
done

if [ "$HELP" == true ]; then
    help
    exit
fi


# push
if [ "$RECORD" == true ]; then
    cluster_fifo --push="$2" --record "$1"
else
    cluster_fifo --push="$2" "$1"
fi

# exit with push exit status
exit $?

