#!/bin/bash

SCRIPT_NAME="$(basename "$0")"
# SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"

# script constants
DISABLE_FILE_EXT="stop"

function usage {
    echo "${SCRIPT_NAME} [--help] parameter_file"
    echo "disables popping new parameter sets from file parameter_file."
}

function help {
    usage
    echo ""
    echo "-h / --help    Show this help text."
    echo "-q / --quiet   Quiet mode."
}

# ui
HELP=false
QUIET=false
## parser loop
while [[ "$1" == -* ]]; do
    case "$1" in
        -h | --help)
	    HELP=true;;
	-q | --quiet)
	    QUIET=true;;
	--*)
	    echo "Unknown option: $1"; exit;;
        --)
	    shift; break;; # end of options
    esac
    shift
done

if [ "$HELP" == true ]; then
    help
    echo ""
    echo "-h / --help    Show this help text."
    echo "-q / --quiet   Quiet mode."
    exit
fi

# get parameter file properties
PARAMETERFILE_DIR="$(cd "$(dirname "$1")" && pwd)"
PARAMETERFILE_NAME="$(basename "$1")"
PARAMETERFILE_BASE=${PARAMETERFILE_NAME%%.*}

# check for parameter_file
if [ -f "${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME}" ]; then
    if [ "$QUIET" == false ]; then
	echo "Parameter popping stopped for ${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME}."
    fi
    touch "${PARAMETERFILE_DIR}/${PARAMETERFILE_BASE}.${DISABLE_FILE_EXT}"
else
    if [ "$QUIET" == false ]; then
	echo "Parameter file ${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME} not found."
    fi
fi
