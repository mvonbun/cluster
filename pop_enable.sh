#!/bin/bash

SCRIPT_NAME="$(basename "$0")"
# SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"

# script constants
DISABLE_FILE_EXT="stop"

function usage {
    echo "${SCRIPT_NAME} [--help, --quiet] parameter_file"
    echo "(re)enables popping new parameter sets from file parameter_file."
}

function help {
    usage
    echo ""
    echo "-h / --help    Show this help text."
    echo "-q / --quiet   Quiet mode."
}


# ui
HELP=false
QUIET=false
## parser loop
while [[ "$1" == -* ]]; do
    case "$1" in
        -h | --help)
	    HELP=true;;
	-q | --quiet)
	    QUIET=true;;
	--*)
	    echo "Unknown option: $1"; exit;;
        --)
	    shift; break;; # end of options
    esac
    shift
done

if [ "$HELP" == true ]; then
    help
    exit
fi

# get parameter file properties
PARAMETERFILE_DIR="$(cd "$(dirname "$1")" && pwd)"
PARAMETERFILE_NAME="$(basename "$1")"
PARAMETERFILE_BASE=${PARAMETERFILE_NAME%%.*}

# check for parameter_file
if [ -f "${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME}" ]; then
    if [ -f "${PARAMETERFILE_DIR}/${PARAMETERFILE_BASE}.${DISABLE_FILE_EXT}" ]; then
	if [ "$QUIET" == false ]; then
	    echo "Parameter popping reenabled for ${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME}."	
	fi
	rm "${PARAMETERFILE_DIR}/${PARAMETERFILE_BASE}.${DISABLE_FILE_EXT}"
    else
	if [ "$QUIET" == false ]; then
	    echo "Parameter popping from ${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME} not disabled."
	fi
    fi
else
    if [ "$QUIET" == false ]; then
	echo "Parameter file ${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME} not found."
    fi
fi
