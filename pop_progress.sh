#!/bin/bash

SCRIPT_NAME="$(basename "$0")"
# SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"

SLEEP_TIME=60

function usage {
    echo "${SCRIPT_NAME} [--help] parameter_file"
    echo "continously prints the progress from file parameter_file."
}

function help {
    usage
    echo ""
    echo "-h / --help    Show this help text."
}

function update {
    REM_LINES_NEW=$(wc -l < "$1")
    POPPED_LINES=$((INIT_REM_LINES - REM_LINES_NEW))
}

# ui
HELP=false
## parser loop
while [[ "$1" == -* ]]; do
    case "$1" in
        -h | --help)
	    HELP=true;;
	--*)
	    echo "Unknown option: $1"; exit;;
        --)
	    shift; break;; # end of options
    esac
    shift
done

if [ "$HELP" == true ]; then
    help
    exit
fi

# main
INIT_REM_LINES=$(wc -l < "$1")
echo "Starting to monitor ${1}"

update "$1"
REM_LINES_OLD=0

while [ "$REM_LINES_NEW" -gt 0 ]; do
    if [ "$REM_LINES_NEW" -ne "$REM_LINES_OLD" ]; then
	echo ">> ${POPPED_LINES} / ${INIT_REM_LINES} parameters popped"
	REM_LINES_OLD=$((REM_LINES_NEW))
    fi
    sleep $SLEEP_TIME
    update "$1"
done

echo ">> all done"
