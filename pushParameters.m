function [ stat, para ] = pushParameters( values, filename, varargin )
%PUSHPARAMETERS Push parameters to the stack using the system cluster
%scripts.
%
%   [ stat, para ] = PUSHPARAMETERS( values, filename, [record<true/false>] )
%   return the status of the push attempt of values to file filename.
%
%   Get the cluster scripts from
%   bitbucket.org:mvonbun/cluster.git
%
%   See also: getParameters
strvalues = @(x) [sprintf('%d', x(1)), sprintf(' %d', x(2:end))];
record = true;
if nargin == 3
    record = varargin{1};
end

if record
    push_parameters_call = ['cluster_fifo', ...
        ' --push="', strvalues(values), '"', ...
        ' --record "', filename, '"'];
else
    push_parameters_call = ['cluster_fifo', ...
        ' --push="', strvalues(values), '"', ...
        ' "', filename, '"'];
end
[stat, para] = system(push_parameters_call);
end

