#!/bin/bash

# For use with screen or clusterssh on remote machines.
# Usage: push_data.sh subfolder

LOCALUSER="mivo"
LOCALDIR="/home/mivo/dev/noc/omnet/bufferless/fountain_noc/simulations/results/UniformFlag/${1}"

REMOTEUSER="ne45paz"
REMOTEDIR="/scratch/ne45paz/fountain_noc/simulations/results/UniformFlag/current/"

OPTIONS="-aunvP"

rsync ${OPTIONS} "${REMOTEDIR}/*.sca" "${LOCALUSER}@${SSH_CONNECTION%% *}:${LOCALDIR}"

