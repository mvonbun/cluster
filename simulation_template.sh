#!/bin/bash

SCRIPT_NAME="$(basename "$0")"
cd "$(dirname "$0")"

function usage {
    echo "Usage: ${SCRIPT_NAME}" \
	 "[--number NUM]" \
	 "[--attempts NUM]" \
	 "[--dry]" \
	 "parameter_file" \
	 "[simulation_program_options]"
}

function help {
    usage
    echo ""
    echo "Options"
    echo "-n / --number NUM     " \
	 "Number of parameters to execute." \
	 "By default, a new parameter is executed as long as the parameter_file" \
	 "is non-empty."
    echo "-a / --attempts NUM   " \
	 "If program fails executing the current parameter," \
	 "it will try again NUM times." \
	 "If all attempts fail, the scripts terminates with error code 64" \
	 "[Number of attempts reached]."
    echo "--dry                 " \
	 "Dry run (do not execute simulation)."
    echo ""
    echo "Example:"
    echo "   ./${SCRIPT_NAME} runs.txt"
}


### simple ui (no POSIX support)
HELP=false
MAX_ATTEMPTS=1
NUMBER=-1
DRY=false
## parser loop
while [[ "$1" == -* ]]; do
    case "$1" in
        -h | --help)
	    HELP=true;;
	-a | --attempts)
	    MAX_ATTEMPTS="$2"; shift;;
	-n | --number)
	    NUMBER="$2"; shift;;
	--dry)
	    DRY=true;;
	-*)
	    echo "Invalid input. Unknown option ${1}";;
        --)
	    shift; break;; # end of options
    esac
    shift
done


# process input
if [ "$HELP" == true ]; then
    help
    exit
fi

## ensure TWO mandatory inputs after the options
if [ "$#" -lt 2 ]; then
    echo "Invalid input. Not enough arguments."
    echo ""
    usage
    exit
fi

## set mandatory inputs
PARAMETERFILE_DIR="$(cd "$(dirname "$1")" && pwd )"
PARAMETERFILE_NAME="$(basename "$1")"
PARAMETERFILE="${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME}"
DONEFILE="${PARAMETERFILE_DIR}/${PARAMETERFILE_NAME%%.*}_done.log"
shift

# main
PARAM="enable"
DOCKERSPAWNED=false
while [ "$PARAM" != "" ] && [ "$NUMBER" -ne 0 ]; do
    let NUMBER-=1

    ## get next parameter from PARAMETERFILE
    PARAM=$(get_parameter --record "$PARAMETERFILE")
    ATTEMPTS=0
    
    ## run simulation
    if [ "$PARAM" != "" ]; then

	while [ "$ATTEMPTS" -lt "$MAX_ATTEMPTS" ]; do
	    let ATTEMPTS+=1

	    # --------------- # BEGIN SIMULATION COMMAND(S) # --------------- #
	    if [ "$DRY" == true ]; then
		# [optional]
		# add command to be executed here (mainly for debugging)
		echo "Dry run"
	    else
		# add the simulation commands here
		# use "${PARAM}" to pass the current parameter to your simulation and
		#     "$*" / "$@" to pass the remaining parameters provided to your simulation
		# e.g. using docker
		# sudo docker run -v "$(pwd)":/sim YOUR_CONTAINER bash -c "YOUR_SIMULATION ${PARAM} $*"
		YOUR_SIMULATION_SCRIPT "${PARAM}" "$@"
	    fi
	    # --------------- # END SIMULATION COMMAND(S) # --------------- #
	    EXIT_STATE=$?

	    if [ "$EXIT_STATE" -eq 0 ]; then
		# push successful attempt to record file
		ATTEMPTS=$MAX_ATTEMPTS
		DONE_PUSHLINE="DONE ${PARAM} ${USER}@${HOSTNAME} $(date +"%Y-%m-%d %H:%M:%S")"
		cluster_fifo --push="$DONE_PUSHLINE" "$DONEFILE"

	    else
		# error
		echo "Simulation terminated with exit status $EXIT_STATE."
		echo "Parameter \"${PARAM}\" was not processed."

		# catch SIGKILL script termination
		# http://tldp.org/LDP/abs/html/exitcodes.html
		# 130: CTRL-C, 137: docker kill/stop pid
		if [ "$EXIT_STATE" -eq 130 ] || [ "$EXIT_STATE" -eq 137 ]; then
		    # push popped parameter back
		    cluster_fifo --push="$PARAM" --record "$PARAMETERFILE"
		    echo "User terminated execution"
		    exit $EXIT_STATE

		else
		    if [ "$ATTEMPTS" -eq "$MAX_ATTEMPTS" ]; then
			# push popped parameter back
			cluster_fifo --push="$PARAM" --record "$PARAMETERFILE"
			echo "Terminating to prevent popping more parameters" \
			     "that can not be processed."
			exit 64

		    else
			echo "Retrying to execute parameter \"${PARAM}\"."
		    fi
		fi
	    fi
	done
    fi


    # post simulation action
    # clean up exited containers (do not quote to have one-line return values)
    # docker rm -v $(docker ps -a -q -f status=exited)
    # echo "docker rm -v \$(docker ps -a -q -f status=exited)"

done


